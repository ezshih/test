#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "cards.h"
using namespace std;

// Global constants (if any)


// Non member functions declarations (if any)


// Non member functions implementations (if any)


// Stub for main
int main() {
	/* --STATEMENTS-- */
	/* --STATEMENTS-- */
	srand(time(0));
	/*Hand yourhand;
	for (int i = 0; i<5; i++)
	yourhand.addcard();
	yourhand.getcard(0);*/
	//yourhand.viewhand();

	Player Human(100);
	Player Dealer(1000000);
	ofstream myfile;
	myfile.open("log1.txt");
	while (Human.viewmoney() > 0)
	{
		

		int bet;
		char yorn = 'y';

		myfile << "You have $" << Human.viewmoney() << " Enter bet: ";
		cout << "You have $" << Human.viewmoney() << " Enter bet: ";

		cin >> bet;
		myfile << "Your cards:" << endl;
		cout << "Your cards:" << endl;

		Human.lookathand();
		while (yorn == 'y')
		{
			myfile << "Your total is " << Human.score() << ". Do you want another card (y/n)?";
			cout << "Your total is " << Human.score() << ". Do you want another card (y/n)?";

			cin >> yorn;
			if (yorn == 'y')
			{
				myfile << "New card:" << endl;
				cout << "New card:" << endl;

				Human.draw();
				myfile << endl << "Your Cards:" << endl;
				cout << endl << "Your Cards:" << endl;

				Human.lookathand();
			}
		}
		myfile << "Dealer's cards: ";
		cout << "Dealer's cards: ";

		Dealer.lookathand();
		myfile << "The dealer's total is " << Dealer.score() << "." << endl;
		cout << "The dealer's total is " << Dealer.score() << "." << endl;

		
		if (Human.score() <= 7.5 && (Human.score() > Dealer.score() || Dealer.score() > 7.5))
		{
			Human.changemoney(bet);
			myfile << "You win " << bet << "." << endl;
			cout << "You win " << bet << "." << endl;

			
		}
		else if (Human.score() <= Dealer.score())
		{
			Human.changemoney(0 - bet);
			myfile << "Too bad. You lose " << bet << ".";
			cout << "Too bad. You lose " << bet << ".";

			if (Human.viewmoney() <= 0)
			{
				myfile << " GAME OVER!" << endl << "Come back when you have more money." << endl << endl << "Bye!";
				cout << " GAME OVER!" << endl << "Come back when you have more money." << endl << endl << "Bye!";
			}

		}

		Human.restart();
		Dealer.restart();

	}
	myfile.close();

}
